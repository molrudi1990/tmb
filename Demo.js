const { chromium } = require('playwright');
const express = require("express");

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://www.tmbbank.com/home
  await page.goto('https://www.tmbbank.com/home');

  await page.goto('https://www.tmbdirect.com/tmb/kdw1.30.1');
  
  // ---------------------
  await context.close();
  await browser.close();

})();
