const { chromium, firefox } = require('playwright');

describe('angularjs homepage todo list', function () {
var todoList = element.all(by.repeater('todo in todoList.todos'));

it('should add a todo', function () {
    browser.get('https://angularjs.org');

    element(by.model('todoList.todoText')).sendKeys('write first protractor test');
    element(by.css('[value="add"]')).click();
});

it('test 2', function () {
    expect(todoList.count()).toEqual(3);
});

it('test 3', function () {
    expect(todoList.get(2).getText()).toEqual('write first protractor test');
});

it('test 4', function () {
    // You wrote your first test, cross it off the list
    todoList.get(2).element(by.css('input')).click();
    var completedAmount = element.all(by.css('.done-true'));
    expect(completedAmount.count()).toEqual(2);
});
});