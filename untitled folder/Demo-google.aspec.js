const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://www.google.com/?gws_rd=ssl
  await page.goto('https://www.google.com/?gws_rd=ssl');

  // Click [aria-label="ค้นหา"]
  await page.click('[aria-label="ค้นหา"]');

  // Fill [aria-label="ค้นหา"]
  await page.fill('[aria-label="ค้นหา"]', 'playw');

  // Click text=playwright
  await page.click('text=playwright');
  // assert.equal(page.url(), 'https://www.google.com/search?q=playwright&source=hp&ei=nQ1bYKvdIJSQ4-EPib2dqAM&iflsig=AINFCbYAAAAAYFsbrZUnsDg5cnt7NaG8Z00Q3oeGGOGn&oq=playw&gs_lcp=Cgdnd3Mtd2l6EAEYADICCAAyCAgAEMcBEK8BMgIIADICCAAyAggAMgIIADIECAAQCjIICAAQxwEQrwEyAggAMgQIABAKOgUIABCxAzoICAAQsQMQgwE6CwgAELEDEMcBEKMCOgUILhCxAzoFCAAQyQNQ8hNYji1gz01oAXAAeACAAekBiAGSBJIBBTQuMC4xmAEAoAEBqgEHZ3dzLXdperABAA&sclient=gws-wiz');

  // Go to https://www.google.com/search?q=playwright&source=hp&ei=nQ1bYKvdIJSQ4-EPib2dqAM&iflsig=AINFCbYAAAAAYFsbrZUnsDg5cnt7NaG8Z00Q3oeGGOGn&oq=playw&gs_lcp=Cgdnd3Mtd2l6EAEYADICCAAyCAgAEMcBEK8BMgIIADICCAAyAggAMgIIADIECAAQCjIICAAQxwEQrwEyAggAMgQIABAKOgUIABCxAzoICAAQsQMQgwE6CwgAELEDEMcBEKMCOgUILhCxAzoFCAAQyQNQ8hNYji1gz01oAXAAeACAAekBiAGSBJIBBTQuMC4xmAEAoAEBqgEHZ3dzLXdperABAA&sclient=gws-wiz
  await page.goto('https://www.google.com/search?q=playwright&source=hp&ei=nQ1bYKvdIJSQ4-EPib2dqAM&iflsig=AINFCbYAAAAAYFsbrZUnsDg5cnt7NaG8Z00Q3oeGGOGn&oq=playw&gs_lcp=Cgdnd3Mtd2l6EAEYADICCAAyCAgAEMcBEK8BMgIIADICCAAyAggAMgIIADIECAAQCjIICAAQxwEQrwEyAggAMgQIABAKOgUIABCxAzoICAAQsQMQgwE6CwgAELEDEMcBEKMCOgUILhCxAzoFCAAQyQNQ8hNYji1gz01oAXAAeACAAekBiAGSBJIBBTQuMC4xmAEAoAEBqgEHZ3dzLXdperABAA&sclient=gws-wiz');

  // Click text=Playwright: Fast and reliable end-to-end testing for modern ...
  await page.click('text=Playwright: Fast and reliable end-to-end testing for modern ...');
  // assert.equal(page.url(), 'https://playwright.dev/');

  // Click text=Get started
  await page.click('text=Get started');
  // assert.equal(page.url(), 'https://playwright.dev/docs/intro');

  // Click text=InstallationUsageFirst scriptRecord scriptsTypeScript supportSystem requirements >> a
  await page.click('text=InstallationUsageFirst scriptRecord scriptsTypeScript supportSystem requirements >> a');
  // assert.equal(page.url(), 'https://playwright.dev/docs/intro#installation');

  // Click text=installation parameters
  await page.click('text=installation parameters');
  // assert.equal(page.url(), 'https://playwright.dev/docs/installation');



  // ---------------------

  test('should click search', async () => {
    await page.goto('https://www.wikipedia.org/');

  await context.close();
  await browser.close();

});

  
})();

