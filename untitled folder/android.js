const { android } = require('playwright-android');

(async () => {
  const [device] = await android.devices();
  console.log(`Model: ${device.model()}`);
  console.log(`Serial: ${device.serial()}`);

  await device.shell('am force-stop org.chromium.webview_shell');
  await device.shell('am start org.chromium.webview_shell/.WebViewBrowserActivity');

  const webview = await device.webView({ pkg: 'org.chromium.webview_shell' });
  const page = await webview.page();

  await device.fill({ res: 'org.chromium.webview_shell:id/url_field' }, 'github.com/microsoft/playwright');
  await Promise.all([
    page.waitForNavigation(),
    device.press({ res: 'org.chromium.webview_shell:id/url_field' }, 'Enter')
  ]);
  console.log(await page.title());

  {
    const context = await device.launchBrowser();
    const [page] = context.pages();
    await page.goto('https://webkit.org/');
    console.log(await page.evaluate(() => window.location.href));
    await context.close();
  }

  await device.close();
})();