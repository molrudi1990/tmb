const { webkit, devices } = require('playwright');

(async() => {
  const browser = await webkit.launch({headless: false});
  const context = await browser.newContext({...devices['iPhone 11 Pro']});
  const page = await context.newPage();
  const { chromium } = require('playwright');

  (async () => {
    const browser = await chromium.launch({
      headless: false
    });
    const context = await browser.newContext();
  
    // Open new page
    const page = await context.newPage();
  
    // Go to https://shopee.co.th/
    await page.goto('https://shopee.co.th/');
  
    // Click button:has-text("ไทย")
    await page.click('button:has-text("ไทย")');
  
    // Click #modal path
    await page.click('#modal path');
  
    // Click text=เข้าสู่ระบบ
    await page.click('text=เข้าสู่ระบบ');
    // assert.equal(page.url(), 'https://shopee.co.th/buyer/login?next=https%3A%2F%2Fshopee.co.th%2F');
  
    // Click button:has-text("Facebook")
    const [page1] = await Promise.all([
      page.waitForEvent('popup'),
      page.click('button:has-text("Facebook")')
    ]);
  
    // Click input[name="email"]
    await page1.click('input[name="email"]');
  
    // Fill input[name="email"]
    await page1.fill('input[name="email"]', '0945513454');
  
    // Click input[name="pass"]
    await page1.click('input[name="pass"]');
  
    // Fill input[name="pass"]
    await page1.fill('input[name="pass"]', 'b5221600170');
  
    // Click text=เข้าสู่ระบบ
    await Promise.all([
      page1.waitForNavigation(/*{ url: 'https://web.facebook.com/v9.0/dialog/oauth?app_id=721805524607838&cbt=1617248545625&channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df258aba92e6178c%26domain%3Dshopee.co.th%26origin%3Dhttps%253A%252F%252Fshopee.co.th%252Ff155999872ee4f8%26relation%3Dopener&client_id=721805524607838&display=popup&domain=shopee.co.th&e2e=%7B%7D&fallback_redirect_uri=https%3A%2F%2Fshopee.co.th%2Fbuyer%2Flogin%3Fnext%3Dhttps%253A%252F%252Fshopee.co.th%252F&locale=en_US&logger_id=fd1e3ff1553b34&origin=1&redirect_uri=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df328110edbd3edc%26domain%3Dshopee.co.th%26origin%3Dhttps%253A%252F%252Fshopee.co.th%252Ff155999872ee4f8%26relation%3Dopener%26frame%3Df3cf50aec9f291&response_type=token%2Csigned_request%2Cgraph_domain&scope=email&sdk=joey&version=v9.0&ret=login&fbapp_pres=0&tp=unspecified&ext=1617252162&hash=AebDnYzpC-M8292Zxo8' }*/),
      page1.click('text=เข้าสู่ระบบ')
    ]);
  
    // Close page
    await page1.close();
  
    // Go to https://shopee.co.th/
    await page.goto('https://shopee.co.th/');
  
    // Click #modal svg
    await page.click('#modal svg');
  
    // Click [placeholder="ค้นหาสินค้าและร้านค้า"]
    await page.click('[placeholder="ค้นหาสินค้าและร้านค้า"]');
  
    // Fill [placeholder="ค้นหาสินค้าและร้านค้า"]
    await page.fill('[placeholder="ค้นหาสินค้าและร้านค้า"]', 'โต๊ะทำงาน');
  
    // Press Enter
    await page.press('[placeholder="ค้นหาสินค้าและร้านค้า"]', 'Enter');
    // assert.equal(page.url(), 'https://shopee.co.th/search?keyword=%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%97%E0%B8%B3%E0%B8%87%E0%B8%B2%E0%B8%99');
  
    // Click text=โต๊ะ (13พัน+)
    await page.click('text=โต๊ะ (13พัน+)');
    // assert.equal(page.url(), 'https://shopee.co.th/search?facet=9255&keyword=%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%97%E0%B8%B3%E0%B8%87%E0%B8%B2%E0%B8%99&noCorrection=true&page=0');
  
    // Click text=EllaMall โต๊ะ โต๊ะทำงาน โต๊ะคอมพิวเตอร์ โต๊ะทำงานไม้ โต๊ะคอม โต๊ะไม้ โต๊ะสำนักงา
    await page.click('text=EllaMall โต๊ะ โต๊ะทำงาน โต๊ะคอมพิวเตอร์ โต๊ะทำงานไม้ โต๊ะคอม โต๊ะไม้ โต๊ะสำนักงา');
    // assert.equal(page.url(), 'https://shopee.co.th/EllaMall-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%97%E0%B8%B3%E0%B8%87%E0%B8%B2%E0%B8%99-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%84%E0%B8%AD%E0%B8%A1%E0%B8%9E%E0%B8%B4%E0%B8%A7%E0%B9%80%E0%B8%95%E0%B8%AD%E0%B8%A3%E0%B9%8C-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%97%E0%B8%B3%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B9%84%E0%B8%A1%E0%B9%89-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%84%E0%B8%AD%E0%B8%A1-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B9%84%E0%B8%A1%E0%B9%89-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%AA%E0%B8%B3%E0%B8%99%E0%B8%B1%E0%B8%81%E0%B8%87%E0%B8%B2%E0%B8%99-%E0%B9%82%E0%B8%95%E0%B9%8A%E0%B8%B0%E0%B8%84%E0%B8%AD%E0%B8%A1Computer-Desk-Home-Office-table-i.295886484.5161430493');
  
    // Click text=น้ำตาล ย80xก40xส72
    await page.click('text=น้ำตาล ย80xก40xส72');
  
    // Click button:has-text("เพิ่มไปยังรถเข็น")
    await page.click('button:has-text("เพิ่มไปยังรถเข็น")');
  
    // Click div[role="button"]:has-text("2")
    await Promise.all([
      page.waitForNavigation(/*{ url: 'https://shopee.co.th/cart' }*/),
      page.click('div[role="button"]:has-text("2")')
    ]);
  
    // Click button:has-text("สั่งสินค้า")
    await Promise.all([
      page.waitForNavigation(/*{ url: 'https://shopee.co.th/checkout-page' }*/),
      page.click('button:has-text("สั่งสินค้า")')
    ]);
  
    // ---------------------
    //await context.close();
    //await browser.close();
  })();