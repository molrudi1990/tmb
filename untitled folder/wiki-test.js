 const { chromium, firefox } = require('playwright');

let browser;
let page;

beforeAll(async () => {
    browser = await chromium.launch({
        headless: true
    });
});

afterAll(async () => {
    await browser.close();
});

beforeEach(async () => {
    page = await browser.newPage();
});

afterEach(async () => {
    await page.close();
});

test('should open wiki page', async () => {
    await page.goto('https://www.wikipedia.org/');
    const url = page.url();

    console.log(url);
});

test('should click search', async () => {
    await page.goto('https://www.wikipedia.org/');

    // Click input[name="search"]
    await page.click('input[name="search"]');
  
    // Press CapsLock
    await page.press('input[name="search"]', 'CapsLock');
  
    // Fill input[name="search"]
    await page.fill('input[name="search"]', 'Agile');
  
    // Click h3:has-text("Agile")
    await page.click('h3:has-text("Agile")');
    // assert.equal(page.url(), 'https://en.wikipedia.org/wiki/Agile');
  
    // Click text=Agile software development
    await page.click('text=Agile software development');

    const text = await page.$eval('#frmIBPreLogin_btnLogIn', e => e.value);
  //expect(text).toBe('เข้าสู่ระบบ');
  

});
